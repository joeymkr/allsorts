/**
 * Created by Joe Roberts on 21/04/2015.
 */
import javax.swing.*;
import java.io.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ProductMaker {

    private String title;
    private String code;
    private static ArrayList<String> category;
    private String brand;
    private String price;
    private String desc;
    private String image;
    private String stock;

    PrintWriter out;

    public static void main(String[] args){
        category = new ArrayList<String>();
        ProductMaker maker = new ProductMaker();
    }

    public ProductMaker(){
        boolean running = true;

        while (running){
            try {
                out = new PrintWriter(new BufferedWriter(new FileWriter("products.txt", true)));
            } catch (IOException e) {
                e.printStackTrace();
            }

            category = new ArrayList<String>();
            title = JOptionPane.showInputDialog(null, "title");
            code = JOptionPane.showInputDialog(null, "code");
            boolean add = true;
            while (add){
                String genre = (JOptionPane.showInputDialog(null, "category"));
                category.add(genre);
                int addAnother = JOptionPane.showConfirmDialog(null, "add another");
                if(addAnother == JOptionPane.NO_OPTION){
                    add = false;
                }
            }

            brand = JOptionPane.showInputDialog(null, "brand");
            price = JOptionPane.showInputDialog(null, "price");
            desc = JOptionPane.showInputDialog(null, "desc");
            image = JOptionPane.showInputDialog(null, "image");
            stock = JOptionPane.showInputDialog(null, "stock");

            appendFile();

            int finished = JOptionPane.showConfirmDialog(null, "add another");
            if (finished != JOptionPane.YES_OPTION){
                running = false;
            }
        }
    }

    private void appendFile(){
        String firstHalf = "," +
                        "\n{" +
                        "\n\"title\": \"" + title  + "\"," +
                        "\n\"code\": \"" + code + "\"," +
                        "\n\"categories\":[";

        String middle = "";
        Iterator it = category.iterator();
        while (it.hasNext()){
            middle += "\n{" + "\n\"category\": \"" + it.next() + "\"}";
            if(it.hasNext()){
                middle +=",";
            }
        }

        String end =    "\n]," +
                        "\n\"brand\": \"" + brand + "\"," +
                        "\n\"price\": \"" + price + "\"," +
                        "\n\"description\": \"" + desc + "\"," +
                        "\n\"image\": \"" + image + ".jpg\"," +
                        "\n\"stock\": \"" + stock + "\""
                        + "\n}";

        try {
            out.println(firstHalf + middle + end);
            out.close();
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}

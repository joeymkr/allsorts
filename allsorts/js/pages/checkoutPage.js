/**
 * Created by Joe Roberts on 24/04/2015.
 */

function displayConfirmationScreen(details){
    $('#mainHolder').empty();
    $('#mainHolder').append(
            "<h1 id='cartTitle'>Thank You " + details.name + " for your order</h1>" +
            "<h3 id=\"confirmation\">Your order is being processed and will be dispatched shortly.</h3><br>" +
            "<h3 id=\"confirmation\">A confirmation email has been sent to " + details.email + "</h3>"
    )

    var cart = getCart();
    for (var each in cart){
        deleteFromCart(each);
    }
}
function submitForm() {

    var formIsValid = true;
    var customerDetails = {};
    // get name
    customerDetails.name = $('#name').val().trim();
    // get phone
    customerDetails.phone = $('#phone').val().trim();
    //get email
    customerDetails.email = $('#email').val().trim();
    // get line 1
    customerDetails.addLine1 = $('#addLine1').val().trim();
    // get town
    customerDetails.town = $('#town').val().trim();
    // get county
    customerDetails.county = $('#county').val().trim();
    // get postcode
    customerDetails.postcode = $('#postcode').val().trim();
    // get card number
    customerDetails.cardNumber = $('#cardNumber').val().trim();
    // get sec code
    customerDetails.secCode = $('#secCodeNumber').val().trim();

    for(var entry in customerDetails) {
        $('#' + entry + '-warning').text('');
    }

    if(!isValidCardNumber(customerDetails.cardNumber)) {
        $('#cardNumber-warning').text('Not a valid card number.');
        formIsValid = false;
    }
    if(!isValidSecCode(customerDetails.secCode)) {
        $('#secCode-warning').text('Not a valid security number.');
        formIsValid = false;
    }

    if(!isNumber(customerDetails.phone)) {
        $('#phone-warning').text('Not a valid phone number.');
        formIsValid = false;
    }

    //Check if any fields are empty
    for(var entry in customerDetails) {
        if(customerDetails[entry] === '') {
            $('#' + entry + '-warning').text('Woops!. Please fill');
            formIsValid = false;
        }
    }





    if(formIsValid) {
        // address line 2
        customerDetails.addLine2 = $('#addLine2').val().trim();
        // expiry month
        customerDetails.month = $('#ex-month').val().trim();
        // expiry year
        customerDetails.year = $('#ex-year').val().trim();

        displayConfirmationScreen(customerDetails);
    } else {
        $('#form-warning').text('Please check fields with errors');
    }
}

function isNumber(string) {
    var pattern = '[+44]?\\d*';
    var regexObj = new RegExp(pattern);
    return regexObj.test(string);
}
function isValidSecCode(string) {
    var pattern = '^(\\d{3})$';
    var regexObj = new RegExp(pattern);
    return regexObj.test(string);
}

function isValidCardNumber(string) {
    var pattern = '^(\\d{16})$';
    var regexObj = new RegExp(pattern);
    return regexObj.test(string);
}



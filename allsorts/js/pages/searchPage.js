/**
 * Created by Joe Roberts on 23/04/2015.
 */

function displaySearchPage(){
    var type = getId().toUpperCase();

    switch (type){
        case "COMPUTING":
            displayComputing();
            break;
        case "DOMESTIC":
            displayDomestic();
            break;
        case "PHOTOGRAPHY":
            displayPhotography();
            break;
        case "HOME THEATRE":
            displayVision();
            break;
        default :
            break;
    }

    displayProductsInCategory(type);
}

function displaySub(category){
    $('#mainHolder').empty();

    var productDB = getProducts();

    for (var i = 0; i < getDBSize(); i++){

        var item = getDBElement(i);
        var subs = item.categories;

        for (var n = 0; n < subs.length; n++) {
            var value = subs[n].category;
            if (category.toUpperCase() == value.toUpperCase()) {
                displayProductSmall(item);
                break;
            }
        }
    }

}

function displayComputing(){
    $('#filter').append("<ul><li><a>Computing</a></li>" +
                                "<ul id=\"subList\"><li><a id=\"sub\" onclick='displaySub(\"laptop\")'>Laptops</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"printer\")'>Printers</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"office\")'>Office</a></li></ul>" +
                            "<li><a href=\"../../pages/search/?id=domestic\">Domestic</a></li>" +
                            "<li><a href=\"../../pages/search/?id=photography\">photography</a></li>" +
                            "<li><a href=\"../../pages/search/?id=home theatre\">Vision</a></li></ul>"
    )
}

function displayDomestic(){
    $('#filter').append("<ul><li><a href=\"../../pages/search/?id=computing\">Computing</a></li>" +
                            "<li><a >Domestic</a></li>" +
                                "<ul id=\"subList\"><li><a id=\"sub\" onclick='displaySub(\"cooking\")'>cooking</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"kitchen\")'>small kitchen</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"iron\")'>irons</a></li></ul>" +
                            "<li><a href=\"../../pages/search/?id=photography\">photography</a></li>" +
                            "<li><a href=\"../../pages/search/?id=home theatre\">Vision</a></li></ul>"
    )
}

function displayPhotography(){
    $('#filter').append("<ul><li><a href=\"../../pages/search/?id=computing\">Computing</a></li>" +
                            "<li><a href=\"../../pages/search/?id=domestic\">Domestic</a></li>" +
                            "<li><a >photography</a></li>" +
                                "<ul id=\"subList\"><li><a id=\"sub\" onclick='displaySub(\"dslr\")'>DSLR</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"action\")'>Action</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"compact\")'>Compact</a></li></ul>" +
                            "<li><a href=\"../../pages/search/?id=home theatre\">Vision</a></li></ul>"
    )
}

function displayVision(){
    $('#filter').append("<ul><li><a href=\"../../pages/search/?id=computing\">Computing</a></li>" +
                            "<li><a href=\"../../pages/search/?id=domestic\">Domestic</a></li>" +
                            "<li><a href=\"../../pages/search/?id=photography\">photography</a></li>" +
                            "<li><a >Vision</a></li>" +
                                "<ul id=\"subList\"><li><a id=\"sub\" onclick='displaySub(\"tv\")'>TV</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"dvd player\")'>DVD</a></li>" +
                                "<li><a id=\"sub\" onclick='displaySub(\"audio\")'>Audio</a></li></ul></ul>"
    )
}
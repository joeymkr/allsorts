/**
 * Created by Joe Roberts on 22/04/2015.
 */
var homePageProducts = [];

function indexPageContent(){
    getFourRandom();

    for(var i = 0; i < homePageProducts.length; i++) {
        displayProductSmall(homePageProducts[i]);
    }

}

function getFourRandom(){
    var numbers = [0,0,0,0];
    for (var i = 0; i < 4; i++){
        var temp = Math.floor(Math.random() * getDBSize() + 1);
        while(temp == numbers[0] || temp == numbers[1] || temp == numbers[2] || temp == numbers[3] || temp < 0 || temp == getDBSize()){
            temp = Math.floor(Math.random() * getDBSize() + 1);
        }
        numbers[i] = temp
        homePageProducts.push(getDBElement(temp));
    }
}
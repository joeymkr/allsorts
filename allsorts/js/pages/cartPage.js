/**
 * Created by Joe Roberts on 23/04/2015.
 */



function displayCartPage(){
    initCart();
    displayCartTitle();
    var mycart = getCart();
    var totalCost = 0;
    for (var key in mycart){
        var productAdded = getProduct(key);
        var productKey = productAdded.code;
        var quantity = mycart[productKey];
        if (quantity > 0) {
            displayProductInCart(productAdded, quantity);
            totalCost += (parseFloat(productAdded.price) * quantity);
        }else {
            deleteFromCart(productKey);
            goToPage("allsorts/pages/cart/");
        }
    }
    displayTotal(totalCost);
}

function displayCartTitle(){
    $('#mainHolder').append(
            "<h1 id='cartTitle'>Shopping Cart</h1>"
    )
}

function displayTotal(total){
    $('#mainHolder').append(
            "<div id='cartHolder'>" +
            "<a id=\"checkout\" href='" + isCartFull(total) + "'>Checkout</a>" +
            "<h2 id='total'>Total = " + total.toFixed(2) + "</h2>" +
            "</div>"

    )
}

function isCartFull(total){
    if(total > 0){
        return "/allsorts/pages/checkout/";
    }else{
        return "";
    }
}

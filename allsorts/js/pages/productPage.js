/**
 * Created by Joe Roberts on 22/04/2015.
 */
function displayProductPage(){
    var id = getId();

    var product = getProduct(id);

    displayProductFull(product);
}


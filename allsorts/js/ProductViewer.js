/**
 * Created by Joe Roberts on 22/04/2015.
 */

function displayProductsInCategory(category){
    var productDB = getProducts();

    for (var i = 0; i < getDBSize(); i++){

        var item = getDBElement(i);
        var subs = item.categories;

        for (var n = 0; n < subs.length; n++) {
            var value = subs[n].category;
            if (category.toUpperCase() == value.toUpperCase()) {
                $('#mainHolder').append(
                        "<a id=\"productLink\" href=\"/allsorts/pages/product/?id=" + productDB.products[i].code + "\">" +
                        "<div class='productHolder'>" +
                        "<img src='../../images/" + productDB.products[i].image + "' alt='no image'>" +
                        "<h2>" + productDB.products[i].title + "</h2>" +
                        "<h3>£" + productDB.products[i].price + "</h3>" +
                        "<p>" + productDB.products[i].description.substr(0,100) + "...read more</p>" +
                        "</div>" +
                        "</a>"
                );
                break;
            }
        }
    }
}

function displayProductSmall(product){
    $('#mainHolder').append(
        "<a id=\"productLink\" href=\"/allsorts/pages/product/?id=" + product.code + "\">" +
            "<div class='productHolder'>" +
                "<img src='/allsorts/images/" + product.image + "' alt='no image'>" +
                "<h2>" + product.title + "</h2>" +
                "<h3>£" + product.price + "</h3>" +
                "<p>" + product.description.substr(0, 100) + "...read more</p>" +
            "</div>" +
        "</a>"
    )
}

function displayProductInCart(product, quantity){
    var price = parseFloat(product.price);
    $('#mainHolder').append(
        "<div id='cartHolder'>" +
            "<img class='cartImg' src='../../images/" + product.image + "' alt='no image'>" +
            "<h2 id='title'>" + product.title.substr(0,20) + "...</h2>" +
            "<div class=\"eachTotal\"><h3>£" + (price * quantity).toFixed(2) + "</h3></div>" +
            "<button type=\"button\" onclick='deleteFromCart(" + product.code + ");goToPage(\"allsorts/pages/cart/\")'>Delete</button>" +
            "<a onclick='addToCart(" + product.code + ");goToPage(\"allsorts/pages/cart/\")'>" + "<h2 id='plus'>+</h2>" +
            "</a>"+
            "<a onclick=\"removeOne(" + product.code + ")\">" + "<h1>-</h1>" +
            "</a>"+
            "<h4>Qty: " + quantity + "</h4>" +
        "</div>"

    )
}

function displayProductFull(product){
    $('#mainHolder').append(
        "<div id='holderFull'>" +
            "<div class='productHolderFull'>" +
                "<img src='../../images/" + product.image + "' alt='no image'>" +
                "<h1>" + product.title + "</h1>" +
                "<h3>£" + product.price + "</h3>" +
                isInStock(product) +
            "</div>\n" +
            "<div class='descriptionHolder'>" +
                "<p>" + product.description + "</p>" +
            "</div>\n" +
            "</div>\n"

    )
}

function isInStock(product){
    if (product.stock > 0){
        return "<button type=\"button\" onClick=\"addToCart(" + product.code + ")\">Add To Cart</button>"
    } else{
        return "<h3 id='outOfStock'>OUT OF STOCK!</h3>";
    }
}

/**
 * Created by Joe Roberts on 23/04/2015.
 */
var cart = {};

function addToCart(code){
    if (cart[code]+1 > getProduct(code).stock){
        alert("No more stock is available!");
        return;
    }
    if (cart.hasOwnProperty(code)){
        cart[code] += 1;
    }else {
        cart[code] = 1;
    }
    setCookie();
    displayAddConfirm();
}

function displayAddConfirm(){
    var windowVcentre = $(window).height()/2;
    var windowHcentre = $(window).width()/2;

    var elementHcentre = $('#popper').outerWidth()/2;
    var elementVcentre = $('#popper').outerHeight()/2;

    var leftMarg = windowHcentre - elementHcentre;
    var topMarg = windowVcentre - elementVcentre;

    $('#popper').html("<h3>Item Added</h3>");

    $('#popper').css({
        left: leftMarg,
        top: topMarg
    });

    $('#popper').fadeIn("slow").delay(1000);
    $('#popper').fadeOut("slow");
}

function setCookie(){
    document.cookie = "cart = " + JSON.stringify(cart) + ";path=/";
}

function getCart(){
    return cart;
}

function removeOne(code){
    cart[code] -= 1;
    setCookie();
    goToPage("allsorts/pages/cart/");
}

function deleteFromCart(code){
    delete cart[code];
    setCookie();
}

function initCart(){
    var cookie = getCookie("cart");
    if (cookie != ""){
        cart = JSON.parse(cookie);
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return '';
}
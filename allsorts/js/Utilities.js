/**
 * Created by Joe Roberts on 23/04/2015.
 */
function getId() {
    var result = "Not found",
        tmp = [];
//  take query string and split it
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
//      split each property = value
        tmp = items[index].split("=");
//      take the value which is the id
        result = decodeURIComponent(tmp[1]);
    }
    return result;
}

function goToPage($url) {
    document.location = '\\' + $url;
}
/**
 * Created by Joe Roberts on 22/04/2015.
 */
var dbsize;

function getProduct(id){
    if(dbsize == null || dbsize == 0){
        alert("Error accessing Database!")
        return null;
    }
    var db = getProducts();
    for (var i = 0; i < dbsize; i++){
        var productCode = db.products[i].code;
        if (id == productCode){
            return db.products[i];
        }
    }

    return null;
}

function getDBElement(id){
    if(dbsize == null || dbsize == 0){
        alert("Error accessing Database!")
        return null;
    }
    var db = getProducts();
    if (id <= dbsize && id >=0){
        return db.products[id];
    }
    return null;
}

function initProductHandler(){
    var db = getProducts();
    dbsize =  db.products.length;
}

function getDBSize(){
    return dbsize;
}

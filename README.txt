---Welcome to Allsorts---

Computer Science CMT3315 Advanced Web Technologies CourseWork 2
 
Date: 24/04/2015
Student Name: Joe Roberts 
Student ID Number: M00422701 
Campus:  Hendon

The source code is located in the following directory - /allsorts/. The site 
is available to view online at:
	
	precog.org.uk/allsorts/

If you wish to run the site locally, copy the /allsorts/ folder into the root 
directory of your local server and type the following into a browser:

	localhost/allsorts/

NOTE: There is a Java file included in the allsorts folder which was used 
to easily load products into a .json file using a simple UI. It is not part 
of the website.
